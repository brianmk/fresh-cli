an easy to use command line tool for building [fresh](https://bitbucket.org/brianmk/fresh) projects

* * *

installation:

`sudo npm install -g git+https://brianmk@bitbucket.org/brianmk/fresh-cli.git`

* * *

usage:

in your terminal, `cd` to the directory of your *fresh* project and run:

* `fresh` to develop in en_US
* `fresh --locale fr_CA` or `fresh -l fr_CA` to develop in fr_CA
* `fresh --production` or `fresh -p` to compile the code for production
* `fresh --debug` or `fresh -d` to view debug messages
* `fresh --help` or `fresh -h` to view this information