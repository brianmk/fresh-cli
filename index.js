#!/usr/bin/env node

function* emojiGenerator() {
  let index = 0
  while(true) yield index++
}

const exec = require('child_process').exec
  , program = require('commander')
  , emoji = require('node-emoji')
  , moonSpinner = require('cli-moon-spinner')
  , chalk = require('chalk')
  , freshASCII = `
             ******
        ******@@@@@***                                    ********   *****
    ****@@@@////////@@**                              ****@@@@@@@****@@@@@*
  ***@@///////////////@**                           **@@@///////@*@/////@@*
 **@@////////////////@@*******               ********@@@@//////@*@/////@@@*
 **@//////////@/////@@*@@@@@@@**           ***@@//////@@@//////@@@/////@@**
 *@@/////////@@///@@@@////////@@***********@@////////////@/////@@@/////@@**
 *@@@////////@@@@@@/////////////@@@@@@@@@@@///////@///////@////////////@@*
 **@@/////////@//@@/////////////@////////@@//////@@//////@@////////////@@*
 **@@@///////////@@////////////@///////////@/////@@//@@@@@@@///////////@@**
  **@@////////@@@@@/////@@@@@@////@@@@@@////@/////@@@/////@@////@@//////@@*
  **@@/////////@@@//////@@///////////@@@//////@////////////@////@@//////@@*
   **@@//////////@//////@//////@@@/////////@@////@@/////////////@*@@@@@@@@*
   **@@////////////////////////@@@@@@/////@@/////@@@///////////@*@@@@@****
   **@@@////////@///////////////@@@@@@@@@@@////////////////////@@****
    **@@@/////@@@///////@//////////////////////////////@@@@@@@****
     **@@@//@@**@///////@@//////////////////@@/////@@@@@@****
      **@@@*****@@@@@@@@@//////////////@@@@@@@@@@@@@@****
       *****  ********@@@@@@//////@@@@@@@************
                      ***@@@@@@@@@@@*****
                        *************`
      .replace(/\//g, chalk.green('/'))
      .replace(/@/g, chalk.black('@'))
      .replace(/\*/g, chalk.magenta('*'))

console.log(freshASCII)

program
  .option('-l, --locale <locale>', 'locale for rendering (defaults to en_US)')
  .option('-p, --production', 'compile code for production')
  .option('-d, --debug', 'display debug messages')
  .parse(process.argv)
  .outputHelp()



const freshProc = exec(
  'gulp'
    + `${program.locale ? ` --locale ${program.locale}` : ''}`
    + `${program.production ? ' production' : ''}`
    + `${program.debug ? '' : ' --silent'}`
    + ' --colors'
  , (error, stdout, stderr) => {
    if (error) {
      console.error(`  ${emoji.get('skull')}  Execution error\n`)
      console.log(`${program.debug ? error : error.message.replace(/^(.*)$/m, '')}`.trim())
      return
    }
    if(stdout) console.log(`Output: ${stdout}`)
    if(stderr) console.log(`Error: ${stderr}`)
  }
)


moonSpinner({
  textAfter: 'Building app',
  stopText: `  ${emoji.get('hammer')}  Building app... Done`
})(spinner => {
  
  freshProc.stdout.on('data', data => {
    if(spinner.isSpinning()) spinner.stop(true)

    // indent stream
    // process.stdout.write(`  ${data}`)

  })

  freshProc.stdout.pipe(process.stdout) // pipe fresh's console output into this process'
  
})